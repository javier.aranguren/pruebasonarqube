import { Component, OnInit } from '@angular/core';
import { ConexionService } from 'src/app/services/conexion.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.sass']
})
export class ListaComponent implements OnInit {

  items:any;
  item:any = {
    name:'',
    edad: ''
  }
  editarItem:any = {
    name: '',
    edad: ''
  }

  constructor(private conexion: ConexionService ) {
    this.conexion.listaItem().subscribe(item=>{
      this.items = item;
      console.log(this.items);
    });
  }

  ngOnInit() {
  }

  agregar(){
    this.conexion.addItem(this.item);
    this.item.name = '';
    this.item.edad = '';
  }

  eliminar(item){
    this.conexion.eliminarItem(item);
  }

  editar(item){
    this.editarItem = item;
  }

  agregarItemEditado(){
    this.conexion.editarItem(this.editarItem);
  }
}
