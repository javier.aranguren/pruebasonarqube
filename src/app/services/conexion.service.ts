import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

export interface Item { name: string;}

@Injectable()
export class ConexionService {

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  private itemDoc: AngularFirestoreDocument<Item>;

  constructor(private afs:AngularFirestore) {
    this.itemsCollection = afs.collection<Item>('javier'); //el nombre que coloque acá es el de la tabla
    this.items = this.itemsCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Item; /// nombre del interface
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });
  }

  listaItem(){
    return this.items;
  }

  addItem(item: Item){
    this.itemsCollection.add(item);
  }

  eliminarItem(item){
    this.itemDoc = this.afs.doc<Item>(`items/${item.id}`); //// recordar comillas especiales `````
    this.itemDoc.delete();
  }

  editarItem(item){
    this.itemDoc = this.afs.doc<Item>(`items/${item.id}`); //// recordar comillas especiales `````
    this.itemDoc.update(item);
  }

}
